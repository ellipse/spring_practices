package org.misty.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

public class Test {
    public static void main(String[] args) {
        System.out.println("1".hashCode());
        System.out.println("========================");
        System.out.println(hash("1"));
        System.out.println(hash2("1", 3, 1000000));
        System.out.println(hash2("1", 13, 1000000));
        System.out.println("========================");
        System.out.println(murmur3_32(3, "1"));
        System.out.println(murmur3_32(13, "1"));
        System.out.println("========================");
        System.out.println(hash3("1"));
        System.out.println(hash3("2"));
        System.out.println(hash3(new Object()));
        System.out.println("hash4 ========================");
        System.out.println(hash4("1"));
        System.out.println(hash4("2"));
        System.out.println(hash4(new Object()));
        System.out.println("========================");
        var idx1 = getIndexes("1", 10000, 4);
        var idx2 = getIndexes("2", 10000, 4);
        var idx3 = getIndexes("3", 10000, 4);
        System.out.println(Arrays.toString(idx1));
        System.out.println(Arrays.toString(idx2));
        System.out.println(Arrays.toString(idx3));
    }

    static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    static long hash2(Object value, int seed, long cap) {
        int h;
        return (value == null) ? 0 : Math.abs(seed * (cap - 1) & ((h = value.hashCode()) ^ (h >>> 16)));
    }

    static int hash3(Object k) {
        int h = 0;
        if (0 != h && k instanceof String) {
            return murmur3_32(0, (String) k);
        }

        h ^= k.hashCode();

        // This function ensures that hashCodes that differ only by
        // constant multiples at each bit position have a bounded
        // number of collisions (approximately 8 at default load factor).
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    static long hash4(Object k) {
        int h = 0;
        if (k instanceof String) {
            h = murmur3_32(0, (String) k);
        } else {
            h ^= k.hashCode();
            h ^= (h >>> 20) ^ (h >>> 12);
            h ^= (h >>> 7) ^ (h >>> 4);
        }
        return h;
    }

    static long[] getIndexes(Object k, long numBits, int numHashFunctions) {
        long[] idx = new long[numHashFunctions];
        long h = hash4(k);
        for (int i = 0; i < numHashFunctions; i++) {
            idx[i] = Math.abs(h) % numBits;
            h += HASH_INCREMENT;
        }
        return idx;
    }

    private static final int HASH_INCREMENT = 0x61c88647;

    public static int murmur3_32(int seed, String value) {
        byte[] data = value.getBytes();
        int len = data.length;
        int res = seed;
        int pos = 0;
        int temp;
        int i;
        for (i = len; i >= 2; res = res * 5 + -430675100) {
            temp = data[pos++] & '\uffff' | data[pos++] << 16;
            i -= 2;
            temp *= -862048943;
            temp = Integer.rotateLeft(temp, 15);
            temp *= 461845907;
            res ^= temp;
            res = Integer.rotateLeft(res, 13);
        }

        if (i > 0) {
            byte var8 = data[pos];
            temp = var8 * -862048943;
            temp = Integer.rotateLeft(temp, 15);
            temp *= 461845907;
            res ^= temp;
        }

        res ^= len * 2;
        res ^= res >>> 16;
        res *= -2048144789;
        res ^= res >>> 13;
        res *= -1028477387;
        res ^= res >>> 16;
        return res;
    }
}
