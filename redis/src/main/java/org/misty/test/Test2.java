package org.misty.test;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class Test2 {
    public static void main(String[] args) {
        var cmap = new ConcurrentHashMap<String, Object>();

        for (int i = 0; i < 10; i++) {
            new Thread(new FutureTask<String>(() -> {
                Object r = cmap.get("key");
                if (r instanceof String) {
                    System.out.println(Thread.currentThread().getName() + " 达成 " + r);
                    return (String) r;
                }
                while (true) {
                    r = cmap.putIfAbsent("key", Boolean.TRUE);
                    if (r == null) {
                        System.out.println(Thread.currentThread().getName() + " 我抢到了");
                        TimeUnit.MILLISECONDS.sleep(300);
                        cmap.put("key", "ok");
                        break;
                    } else if (r instanceof String) {
                        System.out.println(Thread.currentThread().getName() + " 被别人抢先了 " + r);
                        return (String) r;
                    } else {
                        System.out.println(Thread.currentThread().getName() + " 我没抢到，等待重试 " + r);
                        Thread.yield();
                    }
                }
                return null;
            })).start();
        }
    }
}
