package org.misty.test;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.util.ArrayList;
import java.util.Objects;

public class BloomFilterSample1 {
  public static void main(String[] args) {
    var size = 1000000;
    var bloomFilter = BloomFilter.create(
        Funnels.integerFunnel(),
        size, // 预计插入量
        0.001 // 容错率
    );

    for (var i = 1; i <= size; i++) {
      bloomFilter.put(i);
    }
    var miss = new ArrayList<Integer>();
    for (var i = size + 10000; i < size + 20000; i++) {
      if (bloomFilter.mightContain(i)) {
        miss.add(i);
      }
    }
    System.out.println("误判数量：" + miss.size());

    System.out.println(Objects.hashCode("123"));
    System.out.println(Objects.hash("123"));
  }


}
