package org.misty.spring.redis.controller;

import org.misty.bloomfilter.BloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bloom")
public class BloomController {
    @Qualifier("jedis")
    @Autowired
    private BloomFilter jedisBloomFilter;

    @Qualifier("jedisPool")
    @Autowired
    private BloomFilter jedisPoolBloomFilter;

    @Qualifier("template")
    @Autowired
    private BloomFilter templateBloomFilter;

    @GetMapping("/jedis/{key}")
    public Boolean jedis(@PathVariable String key) {
        var filter = jedisBloomFilter;
        filter.put(key);
        return filter.exists(key);
    }

    @GetMapping("/jedisPool/{key}")
    public Boolean jedisPool(@PathVariable String key) {
        var filter = jedisPoolBloomFilter;
        filter.put(key);
        return filter.exists(key);
    }

    @GetMapping("/template/{key}")
    public Boolean template(@PathVariable String key) {
        var filter = templateBloomFilter;
        filter.put(key);
        return filter.exists(key);
    }
}
