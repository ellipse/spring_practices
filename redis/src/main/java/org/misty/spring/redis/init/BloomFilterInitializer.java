package org.misty.spring.redis.init;

import org.misty.spring.redis.filter.RedisBloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class BloomFilterInitializer {
    @Autowired
    private RedisBloomFilter bloomFilter;

    @PostConstruct
    private void init() {
        for (var i = 1; i <= 10; i++) {
            bloomFilter.put(String.valueOf(i));
        }
    }
}
