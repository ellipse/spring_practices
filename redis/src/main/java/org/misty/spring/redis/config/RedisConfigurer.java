package org.misty.spring.redis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@Configuration
public class RedisConfigurer {
  @Bean
  public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
    RedisTemplate<String, Object> temp = new RedisTemplate<>();
    temp.setConnectionFactory(connectionFactory);
    temp.setKeySerializer(RedisSerializer.string());
    temp.setValueSerializer(RedisSerializer.java());
    temp.afterPropertiesSet();
    return temp;
  }
}
