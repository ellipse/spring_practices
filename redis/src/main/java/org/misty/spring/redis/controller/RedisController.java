package org.misty.spring.redis.controller;

import org.misty.bloomfilter.BloomFilter;
import org.misty.spring.redis.filter.RedisBloomFilter;
import org.misty.spring.redis.filter.RedisBloomFilterTemp;
import org.misty.spring.redis.service.MockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class RedisController {
    private static final AtomicInteger num = new AtomicInteger(1);

    @Autowired
    private MockService service;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisBloomFilterTemp bloomFilter;

    @Qualifier("jedisPool")
    @Autowired
    private BloomFilter jedisBloomFilter;

    @Qualifier("template")
    @Autowired
    private BloomFilter templateBloomFilter;

    @GetMapping("/{id:\\d+}")
    public Object index(@PathVariable int id) {
        return service.findByBloom(id);
    }

    @GetMapping("/lua")
    public String index() {
        redisTemplate.execute(RedisScript.of(RedisBloomFilter.luaScript), List.of("test"), "1", "2", "3", "4", "5");
        return redisTemplate.opsForValue().get("test");
    }

    @GetMapping("/bloom/pipe")
    public String pipe() {
        var id = String.valueOf(num.getAndIncrement());
        bloomFilter.putPipe(id);
        return "ok";
    }

    @GetMapping("/bloom/tx")
    public String tx() {
        var id = String.valueOf(num.getAndIncrement());
        bloomFilter.putTx(id);
        return "ok";
    }

    @GetMapping("/bloom/lua")
    public String lua() {
        var id = String.valueOf(num.getAndIncrement());
        bloomFilter.putLua(id);
        return "ok";
    }

    @GetMapping("/bloom/jedis")
    public String lua2() {
        var id = String.valueOf(num.getAndIncrement());
        jedisBloomFilter.put(id);
        return "ok";
    }

    @GetMapping("/bloom/template")
    public String template() {
        var id = String.valueOf(num.getAndIncrement());
        templateBloomFilter.put(id);
        return "ok";
    }
}
