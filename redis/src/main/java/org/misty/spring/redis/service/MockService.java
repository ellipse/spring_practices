package org.misty.spring.redis.service;

import java.util.Map;

public interface MockService {
    Map<String, Object> findByCache(int id);

    Map<String, Object> findByBloom(int id);
}
