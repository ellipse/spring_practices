package org.misty.locker;

/**
 * @author Misty on 2020-05-31
 */
public interface ILocker {
    String acquire();

    boolean release(String identifier);
}
