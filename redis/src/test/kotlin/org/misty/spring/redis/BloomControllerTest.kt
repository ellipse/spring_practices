package org.misty.spring.redis

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.misty.spring.redis.controller.BloomController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class BloomControllerTest {
    @Autowired
    private lateinit var controller: BloomController;

    @Test
    fun testJedis() {
        assertEquals(controller.jedis("test"), true)
    }

    @Test
    fun testJedisPool() {
        assertEquals(controller.jedisPool("test"), true)
    }

    @Test
    fun testTemplate() {
        assertEquals(controller.template("test"), true)
    }
}
