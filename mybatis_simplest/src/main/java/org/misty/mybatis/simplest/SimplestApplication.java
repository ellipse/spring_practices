package org.misty.mybatis.simplest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = {"org.misty.mybatis.simplest.dao"})
@SpringBootApplication
public class SimplestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimplestApplication.class, args);
    }

}
