package org.misty.mybatis.simplest.model;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Data
@Alias("user")
public class User {
    private Long id;
    private String username;
    private String password;
    private String name;
    private int age;
    private boolean gender;
    private Date birthday;
    private Date createdAt;
    private Date updatedAt;
}
