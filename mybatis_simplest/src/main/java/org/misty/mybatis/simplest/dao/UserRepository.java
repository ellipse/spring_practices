package org.misty.mybatis.simplest.dao;

import org.misty.mybatis.simplest.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {
    User selectUser(Long id);
}
