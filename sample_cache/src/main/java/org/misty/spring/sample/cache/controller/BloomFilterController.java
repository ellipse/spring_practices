package org.misty.spring.sample.cache.controller;

import org.misty.redis.bloomfilter.BloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Misty on 2020-05-31
 */
@RestController
@RequestMapping("/bloom")
public class BloomFilterController {
    private static final AtomicInteger num = new AtomicInteger(1);

    @Autowired
    private BloomFilter userFilter;

    @GetMapping("/test")
    public String test() {
        String id = String.valueOf(num.getAndIncrement());
        userFilter.put(id);
        return "ok";
    }
}
