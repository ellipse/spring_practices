package org.misty.spring.sample.cache.config;

import lombok.Setter;
import org.misty.redis.bloomfilter.BloomFilter;
import org.misty.redis.bloomfilter.BloomFilterConfig;
import org.misty.redis.bloomfilter.client.JedisPoolAdapter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import redis.clients.jedis.JedisPool;

import java.lang.reflect.Field;

/**
 * @author Misty on 2020-05-31
 */
@Configuration
@ConfigurationProperties("bloom.filter")
public class BloomFilterConfigurer {
    @Setter
    private BloomFilterConfig user;

    @Bean
    public JedisPoolAdapter jedisPoolAdapter(JedisConnectionFactory factory) throws Exception {
        Field f = JedisConnectionFactory.class.getDeclaredField("pool");
        f.setAccessible(true);
        JedisPool pool = (JedisPool) f.get(factory);
        return new JedisPoolAdapter(pool);
    }

    @Bean
    public BloomFilter userFilter(JedisPoolAdapter client) {
        return new BloomFilter(client, user);
    }
}
