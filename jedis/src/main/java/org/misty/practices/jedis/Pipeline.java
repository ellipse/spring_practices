package org.misty.practices.jedis;

import redis.clients.jedis.Jedis;

public class Pipeline {
  public static void main(String[] args) {
    var start = 0L;
    var end = 0L;
    try (var jedis = new Jedis("192.168.31.50", 6379)) {
      // without pipeline
      start = System.currentTimeMillis();
      for (var i = 0; i < 10000; i++) {
        jedis.hset("hashkey:" + i, "field" + i, "value" + i);
      }
      end = System.currentTimeMillis();
      System.out.println("Cost " + (end - start) + " ms");

      // with pipeline
      start = System.currentTimeMillis();
      for (var i = 0; i < 100; i++) {
        var pipeline = jedis.pipelined();
        for (var j = i * 100; j < (i + 1) * 100; j++) {
          pipeline.hset("pipeline-hashkey:" + j, "field" + j, "value" + j);
        }
        pipeline.syncAndReturnAll();
      }
      end = System.currentTimeMillis();
      System.out.println("Cost " + (end - start) + " ms");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
