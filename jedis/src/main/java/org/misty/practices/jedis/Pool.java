package org.misty.practices.jedis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.JedisPool;

public class Pool {
  public static void main(String[] args) {
    var config = new GenericObjectPoolConfig<>();
    config.setMaxTotal(10);
    var jedisPool = new JedisPool(config, "192.168.31.50", 7000);
    try (var jedis = jedisPool.getResource()) {
      jedis.set("hello", "world");
      System.out.println(jedis.get("hello"));
      jedis.close();
      // 疑问：jedis归还连接池后依然可以执行操作？
//      jedis.set("hello", "world1");
//      System.out.println(jedis.get("hello"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
