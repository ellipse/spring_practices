package org.misty.practices.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

public class Simple {
    public static void main(String[] args) {
        try (var jedis = new Jedis("192.168.31.50", 6379)) {
//      jedis.set("hello", "world");
//      var value = jedis.get("hello");
//      System.out.println(value);

//      var buf = ByteBuffer.allocate(4);
//      buf.putInt(111);
//      jedis.set("int".getBytes(), buf.array());
//      var outBuf = ByteBuffer.wrap(jedis.get("int".getBytes()));
//      System.out.println(outBuf.getInt()); // out: 111

//      System.out.println(jedis.incr("counter"));
//      System.out.println(jedis.incr("counter"));
//      System.out.println(jedis.get("counter"));

//      jedis.hset("myhash", "f1", "v1");
//      jedis.hset("myhash", "f2", "v2");
//      System.out.println(jedis.hgetAll("myhash"));

//      jedis.rpush("mylist", "1");
//      jedis.rpush("mylist", "2");
//      jedis.rpush("mylist", "3");
//      System.out.println(jedis.lrange("mylist", 0, -1));

//      jedis.sadd("myset", "a");
//      jedis.sadd("myset", "b");
//      jedis.sadd("myset", "c");
//      System.out.println(jedis.smembers("myset"));

            jedis.zadd("myzset", 99, "tom");
            jedis.zadd("myzset", 66, "peter");
            jedis.zadd("myzset", 33, "james");
            System.out.println(jedis.zrangeWithScores("myzset", 0, -1));

            jedis.set("", "", SetParams.setParams());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
