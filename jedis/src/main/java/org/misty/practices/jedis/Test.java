package org.misty.practices.jedis;

import java.beans.IntrospectionException;
import java.beans.Introspector;

public class Test {
  public static void main(String[] args) throws IntrospectionException {
    int x = 1, y = 1;
//    System.out.println(x++);
//    System.out.println(++y);
    int a = x++;
    int b = ++y;
    System.out.println(a + " " + b);
    System.out.println(x + " " + y);

    var cls = Object.class;
    var info = Introspector.getBeanInfo(Object.class);
    info.getPropertyDescriptors();

    System.out.println("ext " + System.getProperty("java.ext.dirs"));

    var cl = Thread.currentThread().getContextClassLoader();
    while (cl != null) {
      System.out.println(cl);
      cl = cl.getParent();
    }
  }
}
