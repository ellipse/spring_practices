package org.misty.practices.jedis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class Sentinel {
  public static void main(String[] args) throws InterruptedException {
    var poolConfig = new GenericObjectPoolConfig<>();
    poolConfig.setMaxTotal(10);
    var sentinels = new HashSet<String>();
    sentinels.add("192.168.31.68:27000");
    sentinels.add("192.168.31.68:27001");
    sentinels.add("192.168.31.68:27002");

    var sentinelPool = new JedisSentinelPool("mymaster", sentinels, poolConfig, 30000);
    try (var jedis = sentinelPool.getResource()) {
      jedis.set("hello", "world");
      System.out.println(jedis.get("hello"));
    } catch (Exception e) {
      e.printStackTrace();
    }

    TimeUnit.HOURS.sleep(1);
  }
}
