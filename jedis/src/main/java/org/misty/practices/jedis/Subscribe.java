package org.misty.practices.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class Subscribe {
  public static void main(String[] args) {
    System.out.println("start");
    var jedis = new Jedis("192.168.31.68", 7001);
    jedis.subscribe(new JedisPubSub() {
      @Override
      public void onMessage(String channel, String message) {
        System.out.println(channel + " : " + message);
        if ("shutdown".equals(message)) {
          unsubscribe();
        }
      }
    }, "aaa");
    System.out.println("end");
  }
}
