#!/bin/bash

nginx -g 'daemon on;' > /app/logs/nginx.log &

java -jar /app/server/demo.jar
