package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Misty on 2020-06-30
 */
@RestController
@RequestMapping("api")
public class DemoController {
    @GetMapping
    public String index() {
        return "你好世界";
    }
}
