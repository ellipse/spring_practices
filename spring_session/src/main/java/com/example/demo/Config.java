package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession
public class Config {
  @Bean
  public JedisConnectionFactory connectionFactory() {
    var standaloneConfig = new RedisStandaloneConfiguration("192.168.31.68", 6379);
    return new JedisConnectionFactory(standaloneConfig);
  }
}
