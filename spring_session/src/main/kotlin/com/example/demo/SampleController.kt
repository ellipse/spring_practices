package com.example.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@RestController
class SampleController {
  @GetMapping("/sample")
  fun index(n: String, request: HttpServletRequest): String {
    val session = request.session
    session.setAttribute("n", n)
    println(session.getAttribute("n"))
    return n;
  }
}
